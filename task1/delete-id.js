"uses strict";

const request = require('request');

//Удалить пользователя с id
let id = 3

let request_url = 'http://localhost:3000/api/v1/users/' + id;

request({
  method: 'DELETE',
  url: request_url,
  json: true,
  headers: {
    'User-Agent': 'request'
  }
}, (err, res, body) => {
  console.log(res.statusCode);
  console.log(res.body);
});