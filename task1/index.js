"uses strict";
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const restAPI = express.Router();
const server_port = 3000;
const content = require('./content.js');
const html_content1  = content.content1;

const conf = {encoding: "utf-8"};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));

let counter = 0;
let users = [
	{
		"id"    : "0",
		"user"  : "Semen",
		"score" : "4"
	},
	{
	    "id"    : "1",
	    "user"  : "Lui",
		"score" : "3" 
	},
	{
	    "id"    : "2",
	    "user"  : "Nikolay",
		"score" : "5" 
	},
	{
	    "id"    : "3",
	    "user"  : "Vinokur",
		"score" : "5" 
	},
	{
	    "id"    : "4",
	    "user"  : "Misha",
		"score" : "4" 
	}
]; 

restAPI.get("/", function(req,res){
  res.send(html_content1);
});

restAPI.get("/users/", function(req, res) {

        res.status(200).json(users);
		res.end();
});	

restAPI.post("/users/", function(req, res) { 
  let post_request = req.body;
  console.log('Поступил POST запрос');
  let post_user = post_request.user;
  console.log("post_request.user", post_request.user);
  let post_score = post_request.score;
  console.log("post_request.score", post_request.score);
  let new_user = {
  	"id"   : users.length,
  	"user" : post_user,
  	"score": post_score 
  };
  users.push(new_user); 
  res.status(200).json(users);
  res.end();
});

restAPI.get("/users/:id", function(req, res) { 
//найти пользователя по id
   console.log(req.params.id);
   res.status(200).json(users[req.params.id]); 
   res.end();

});

restAPI.put("/users/:id", function(req, res) { 
//put идемпотентен
//обновить данные пользователя по id
  console.log("req.body", req.body[0]);
  console.log("req.body.score", req.body[0].score);
  let put_request = req.body;
  users[req.params.id] = {
    "id"     : req.params.id,
    "user"   : req.body[0].user,
    "score"  : req.body[0].score  	
  };
  res.status(200).json(users);
  res.end();

});

restAPI.delete("/users/:id", function(req, res) { 
//удалить пользователя по id
  	console.log(req.params.id);
  	if (req.params.id >= 0 && req.params.id <= (users.length-1)){
	  	users.splice(req.params.id,1);
	    res.status(200).json(users);
	    res.end();
  	} else {
  		//послать ошибку ввода данных
  	}
});

app.use("/api/v1", restAPI);

app.listen(server_port)
