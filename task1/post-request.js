"uses strict";

var request = require('request');

var values = 
  {
    "user": "Ivan",
    "score": "5"
  };
request({
  method: 'POST',
  url: 'http://localhost:3000/api/v1/users/',
  body: values,
  json: true,
  headers: {
    'User-Agent': 'request'
  }
}, (err, res, body) => {
  console.log(res.statusCode);
  console.log(res.body);
});