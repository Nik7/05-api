const fs = require('fs');
const filepath = "./users.json";
const conf = {encoding: "utf-8"};
const JSONStream = require('JSONStream');
const input_fs = fs.createReadStream(filepath);

let fields = ['user', 'score'];

let stream = input_fs.pipe(JSONStream.parse('*'));
let counter = 0;
stream.on('data',(data)=>{
	counter++;
	//console.log("counter = %d", counter);
	console.log("user: %s, score: %s", data[fields[0]], data[fields[1]]);
})


