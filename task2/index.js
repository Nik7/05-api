"uses strict";
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const rpc = express.Router();
const server_port = 3000;
const content = require('./content.js');

const conf = {encoding: "utf-8"};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));

let counter = 0;
let users = [
	{
		"id"    : "0",
		"user"  : "Semen",
		"score" : "4"
	},
	{
	    "id"    : "1",
	    "user"  : "Lui",
		"score" : "3" 
	},
	{
	    "id"    : "2",
	    "user"  : "Nikolay",
		"score" : "5" 
	},
	{
	    "id"    : "3",
	    "user"  : "Vinokur",
		"score" : "5" 
	},
	{
	    "id"    : "4",
	    "user"  : "Misha",
		"score" : "4" 
	}
]; 
let RPC = {};

// ==============не проверено============ 
RPC['get_users'] = function(params, error, result, callback){
  result = users;
};

RPC['add_user'] = function(params, error, result, callback){
  let new_user = {
    "id"   : users.length,
    "user" : params.user,
    "score": params.score 
  };
  users.push(new_user);
  result = users;
};

RPC['find_user'] = function(params, error, result, callback){
  result=users[params.id]
};

RPC['update_user'] = function(params, error, result, callback){
  users[params.id] = {
    "id"     : params.id,
    "user"   : params.user,
    "score"  : params.score    
  };
  result = users;
};

RPC['delete_user'] = function(params, error, result, callback){
      users.splice(params.id,1);
      result = users;
};
// ==============не проверено============

rpc.get("/rpc/", function(req, res) {
  const method = RPC[req.body.method];
  method(req.body.params, function(error, result) {
    if (error){
      res.status(500);
      res.send("Ошибка сервера");
      res.end();
    } else{
      res.status(200).json(result);
      res.end();  
    }
    
  });

});	

app.use("/api/v1", rpc);

app.listen(server_port)
